import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";
import SignUp from "./Pages/SignUp/SignUp";
import CyberBoard from "./Pages/CyberBoard/CyberBoard";
import CreateProject from "./Pages/CreateProject/CreateProject";
import Layout from "./HOC/Layout";
import ProjectManagement from "./Pages/ProjectManagement/ProjectManagement";
import Spinner from "./Components/Spinner/Spinner";
import UserManager from "./Pages/UserManager/UserManager";
import Profiles from "./Pages/Profiles/Profiles";
function App() {
  return (
    <div>
      <Spinner />
      <BrowserRouter>
        <Routes>
          <Route path="/homepage" element={<HomePage />} />
          <Route
            path="/cyberboard"
            element={
              <Layout>
                <CyberBoard />
              </Layout>
            }
          />
          <Route
            path="/projectmanagement"
            element={
              <Layout>
                <ProjectManagement />
              </Layout>
            }
          />
          <Route
            path="/createproject"
            element={
              <Layout>
                <CreateProject />
              </Layout>
            }
          />
          <Route path="/" element={<LoginPage />} />
          <Route path="/signup" element={<SignUp />} />
          <Route path="*" element={<NotFoundPage />} />
          <Route
            path="/users"
            element={
              <Layout>
                <UserManager />
              </Layout>
            }
          ></Route>
          <Route
            path="/profile"
            element={
              <Layout>
                <Profiles />
              </Layout>
            }
          ></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
