import React from 'react'
import { PacmanLoader } from 'react-spinners'
import { useSelector } from 'react-redux';

export default function Spinner() {
    let isLoading = useSelector((state) => {
        return state.SpinnerSlice.isLoading
    })
  return isLoading ? (
    <div className='spinner fixed w-screen h-screen top-0 left-0 z-50 flex justify-center items-center' style={{backgroundColor:"rgba(0, 0 ,0, 0.8)"}}>
        <PacmanLoader size={150} speedMultiplier={2} color="#3C84AB" />
    </div>
  ) : (<></>)
}
