import React from 'react'
import { useSelector } from 'react-redux'
import { userLocalService } from '../../service/localService';

export default function UserNav() {
  // lấy dữ liệu từ redux xuống
  let userLogin = useSelector((state) => {
    return state.userSliceLogin.userLogin;
    });
    console.log("userLogin",userLogin);

    const handleLogout=() => {
      // Xóa dữ liệu từ localStore
      userLocalService.remove();
      window.location.reload();
      window.location.href="/"
    };

    const renderContent=() => {
      if(userLogin) {
        return (
        <>
           <p className='font-medium'>{userLogin?.name}</p>
           <button onClick={handleLogout} className='border-2 border-blue-500 ml-0 px-5 py-2 rounded hover:text-blue-600 font-medium'>Log out</button>
        </>
        )
      }
    }
  return (
    <div className='space-x-3'>
       {renderContent()}
    </div>
  )
}
