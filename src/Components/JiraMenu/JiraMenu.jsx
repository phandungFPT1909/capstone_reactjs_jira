import React from "react";
import User from "./User";
import { NavLink } from "react-router-dom";

export default function JiraMenu() {
  return (
    <div className="menu">
      <div className="account">
        <div className="avatar">
          <img src={require("../../assets/img/download.jfif")} alt />
        </div>
        <div className="account-info">
          <p>CyberLearn.vn</p>
          <p>Report bugs</p>
        </div>
      </div>
      <div className="control">
        <div>
          <i className="fa fa-credit-card" />
          <NavLink to={"/cyberboard"}>Cyber Board</NavLink>
        </div>
        <div>
          <i className="fa fa-cog" />
          <NavLink to={"/projectmanagement"}>Project Management</NavLink>
        </div>
        <div>
          <i className="fa fa-cog" />
          <NavLink to={"/createproject"}>Create Project</NavLink>
        </div>
      </div>
      <div className="feature">
        <div>
          <i class="fa fa-users"></i>
          <NavLink to={"/users"}>Users</NavLink>
        </div>

        <div>
          <i className="fa fa-cog" />
          <NavLink to={"/profile"}>Profile</NavLink>
        </div>
        <div className="user">
          <User />
        </div>
      </div>
    </div>
  );
}
