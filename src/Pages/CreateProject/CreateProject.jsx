import React, { useEffect,useState } from "react";
import { message } from "antd";
import { Editor } from "@tinymce/tinymce-react";
import { connect, useSelector } from 'react-redux';
// import { withFormik } from 'formik';
import { useDispatch } from 'react-redux';
import { getProjectCategory, postCreateProject } from "../../service/createProjectService";
import {setProjectCategoryInfo } from "../../redux-toolkit/ProjectCategorySlice";
// import * as Yup from "yup";
import { useFormik } from 'formik';
import * as Yup from "yup";
import { postCreateProjectAction, setCreateProject } from "../../redux-toolkit/createProjectSlice";
// 
 function CreateProject(props) {
  const dispatch = useDispatch();

  const arrProjectCategory = useSelector((state) => {
    return state.projectCategorySlice.arrProjectCategory
  })
  const [contents, setContent] = useState('');

  console.log("arrProjectCategory",arrProjectCategory);

  const handleEditorChange = (content, editor) => {
    var contentNew = content;
    setContent(contentNew);
    console.log("content",contents);
  };


  useEffect(() => {
    // gọi API để lấy dữ liệu thẻ select
    getProjectCategory()
    .then((res) => {
      dispatch(setProjectCategoryInfo(res.data.content))
    })
    .catch((err) => {
      console.log(err);
     
    })
  }, []);

  const formik = useFormik({
    initialValues: () => {
      return {
      projectName:"",
      description:"",
      categoryId:2,
    }},

    onSubmit: (values) => {
      console.log("values1",values);
      let request = {...values,description:contents }
      console.log("values2",request);

        postCreateProject(request)
      .then((res) => {
        dispatch(setCreateProject(res.data.content))
        console.log("res",res.data.content);
        message.success("thêm thành công");
      })
      .catch((err) => {
        console.log(err);
        message.success("thêm thất bại");
      })
     
    },
  });

  return (
    <div className="main">
      <div className="container m-5">
      <h1 className="text-2xl">Create Project</h1>
      <form className="container" onSubmit={formik.handleSubmit}>
        <div className="form-group">
        <h2 className='text-xl text-blue-600'>Project Name</h2>
          <input className="form-control" name="projectName" onChange={formik.handleChange} value={formik.values.projectName} />
        </div>

        <div className="form-group">
        <h2 className='text-xl text-blue-600'>Project Category</h2>
          <select name="categoryId" className="form-control" onChange={formik.handleChange} value={formik.values.categoryId}>
          {arrProjectCategory.map((item,index) => {
            return <option value={item.id} key={index}>{item.projectCategoryName}</option>
          })}
          </select>
        </div>

        <div className="form-group">
        <h2 className='text-xl text-blue-600'>Description</h2>
          <Editor
             name="description"
            // initialValue=""
            init={{
              height: 380,
              menubar: false,
              plugins: [
                "a11ychecker",
                "advlist",
                "advcode",
                "advtable",
                "autolink",
                "checklist",
                "export",
                "lists",
                "link",
                "image",
                "charmap",
                "preview",
                "anchor",
                "searchreplace",
                "visualblocks",
                "powerpaste",
                "fullscreen",
                "formatpainter",
                "insertdatetime",
                "media",
                "table",
                "help",
                "wordcount",
              ],
              toolbar:
                "undo redo | casechange blocks | bold italic backcolor | " +
                "alignleft aligncenter alignright alignjustify | " +
                "bullist numlist checklist outdent indent | removeformat | a11ycheck code table help",
            }}
            onEditorChange={handleEditorChange}
          />
        </div>
        
        <button className="border-2 btn btn-outline-primary" type="submit">
          Create Project
        </button>
      </form>
    </div>
    </div>

    );
   }

   export default CreateProject
  
  //  dispatch(setCreateProject(res.data.content))


  