import React from "react";

import { Button, Input, Space, Table, message, Modal } from "antd";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";

import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";

import {
  setGetUserInfo,
  setGetUserValue,
} from "../../redux-toolkit/UserManager/UserList";
import FormEditUser from "./FormEditUser";
//import api
import { getUserArr, deletUser, editUser } from "../../service/userManager";

export default function UserManager() {
  const dispatch = useDispatch();
  // Ham mở form chỉnh sữa
  const [selectedUser, setSelectedUser] = useState("");
  const [showEditUserModal, setShowEditUserModal] = useState(false);

  const handleEditUser = (user) => () => {
    setSelectedUser(user);
    console.log(user);
    console.log("Yess");
    setShowEditUserModal(true);
  };

  const handleCancelEditUser = () => {
    setShowEditUserModal(false);
  };

  const arrUserList = useSelector((state) => {
    return state.getUserSlice.arrUserManager;
  });

  // Hàm hiện danh sách người dùng

  useEffect(() => {
    const listUser = () => {
      getUserArr()
        .then((res) => {
          let orderno = 0;
          let arrUserArr = res.data.content.map((item) => {
            orderno++;
            return {
              ...item,
              orderno,
              action: (
                <>
                  <Button
                    onClick={() => {
                      {
                        handleDeleteUser(item?.id);
                      }
                    }}
                  >
                    <DeleteOutlined />
                  </Button>
                  <Button onClick={handleEditUser(item)}>
                    <EditOutlined />
                  </Button>
                </>
              ),
            };
          });

          dispatch(setGetUserInfo(arrUserArr));
        })
        .catch((error) => {
          console.log(error);
        });
    };
    listUser();

    // Hàm xóa người dùng
    const handleDeleteUser = (idUser) => {
      return deletUser(idUser)
        .then((res) => {
          console.log("res: ", res);
          message.success("XÓA Thất bại");
          listUser();
        })
        .catch((error) => {
          message.error("Xóa thất bại");
          console.log(error);
        });
    };
  }, []);

  console.log("arrUserList: ", arrUserList);

  const columns = [
    {
      title: "No.",
      key: "index",
      dataIndex: "orderno",
      sorter: (item1, item2) => item2.orderno - item1.orderno,
    },
    {
      title: "Name",
      key: "name",
      dataIndex: "name",
      sorter: (item2, item1) => {
        let name1 = item1.name?.trim().toLowerCase();
        let name2 = item2.name?.trim().toLowerCase();
        if (name2 < name1) {
          return -1;
        }
        return 1;
      },
    },
    {
      title: "UserId",
      key: "id",
      dataIndex: "userId",
      sorter: (item1, item2) => item2.userId - item1.userId,
    },

    {
      title: "Email",
      key: "email",
      dataIndex: "email",
      sorter: (item2, item1) => {
        let email1 = item1.email?.trim().toLowerCase();
        let email2 = item2.email?.trim().toLowerCase();
        if (email2 < email1) {
          return -1;
        }
        return 1;
      },
    },

    {
      title: "Phone Number",
      key: "phoneNumber",
      dataIndex: "phoneNumber",
      sorter: (item1, item2) => item2.phoneNumber - item1.phoneNumber,
    },

    {
      tile: "Action",
      dataIndex: "action",
      key: "action",
    },
  ];

  return (
    <>
      <div className="main w-full mt-5">
        <Table
          columns={columns}
          rowKey={"userId"}
          dataSource={arrUserList}
        ></Table>
      </div>

      {selectedUser && (
        <FormEditUser
          visible={showEditUserModal}
          onCancel={handleCancelEditUser}
          user={selectedUser}
          handleEditUser
        />
      )}
    </>
  );
}
