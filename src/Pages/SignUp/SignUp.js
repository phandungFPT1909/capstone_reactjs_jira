import React from "react";
import { Button, Checkbox, Form, Input, message, Select } from "antd";
import { postRegister } from "./../../service/userService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import "./styleSign.css";
import { NavLink } from "react-router-dom";
import { setUserInfoSignUp } from "../../redux-toolkit/userSliceSignUp";


export default function SignUpPage() {
  let navigate = useNavigate();
  // dữ liệu lên redux
  let dispatch = useDispatch();
  const onFinish = (values) => {
    // chuyển đổi thành key có tài khoản với mật khẩu trùng trên API
    let user = {
      email: values.email,
      password: values.password,
      name: values.hoTen,
      // gender: values.gender,
      phoneNumber: values.soDt,
    };
    postRegister(user)
      .then((res) => {
         // gửi dữ liệu lên redux
        dispatch(setUserInfoSignUp(res.data.content))
        // chuyển hướng về trang login sau khi đăng ký thành công
        setTimeout(() => {
          navigate("/");
        }, 2000);
        message.success("Đăng ký thành công");
      })

      .catch((err) => {
        message.success("Đăng ký thất bại");
        console.log(err);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
      <div className="signUp flex">
        <div className="signUp-left w-full">
          <img className="h-screen" src="" alt="" />
        </div>
        <div className="px-10 p-5 rounded flex justify-center items-center signUp-right">
          <div className="p-5 rounded signUp-right-content">
          <Form
            layout="vertical"
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
         className="w-full" >
            <Form.Item>
              <div className="signUp__icon">
              <i class="fa fa-user-lock"></i>
              </div>
              <h2 className="mb-0 text-2xl font-medium text-center">Đăng ký</h2>
            </Form.Item>
            
            <Form.Item
              label="Email"
              name="email"
              rules={[
                {
                  required: true,
                  message: "Please input your Email",
                },
              ]}
            >
              <Input placeholder="alice3422@gmail.com" />
            </Form.Item>

            <Form.Item
              label="Mật khẩu"
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              label="Họ và Tên"
              name="hoTen"
              rules={[
                {
                  required: true,
                  message: "Please input your Name",
                },
              ]}
            >
              <Input placeholder="Nguyen Van A" />
            </Form.Item>

            <Form.Item
              label="Phone "
              name="soDt"
              rules={[
                {
                  required: true,
                  message: "Please input your Phone number",
                },
              ]}
            >
              <Input placeholder="0123456789" />
            </Form.Item>

            <Form.Item
              name="remember"
              valuePropName="checked"
              wrapperCol={{
                offset: 0,
                span: 16,
              }}
            >
              <Checkbox>Nhớ tài khoản</Checkbox>
            </Form.Item>

            <Form.Item
              wrapperCol={{
                span: 24,
              }}
              className="text-center"
            >
              <Button
                className="bg-blue-500 text-white px-20 font-medium"
                htmlType="submit"
              >
                Đăng ký
              </Button>
            </Form.Item>

            <Form.Item>
              <NavLink to="/">
                <p className="text-blue-500 text-base font-medium text-center">
                  Bạn đã có tài khoản? Đăng nhập
                </p>
              </NavLink>
            </Form.Item>
          </Form>
          </div>
        </div>
       </div>
  );
}
