import React from 'react'
import HeaderMain from '../../Components/JiraMain/HeaderMain'
import InfoMain from '../../Components/JiraMain/InfoMain'
import ContentMain from './../../Components/JiraMain/ContentMain';

export default function CyberBoard() {
  return (
    <div className='main'>
        <HeaderMain />
        <h3>Cyber Board</h3>
        <InfoMain />
        <ContentMain />
    </div>
  )
}
