import React from "react";
import {
  Avatar,
  Form,
  Input,
  Button,
  message,
  Typography,
  Row,
  Col,
} from "antd";
import { useFormik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { editUser } from "../../service/userManager";
// import { setGetUserValue } from "../../redux-toolkit/UserManager/UserList";
import { propTypesSelected } from "@material-tailwind/react/types/components/select";
export default function Profiles() {
  const dispatch = useDispatch();
  const mainUser = useSelector((state) => state.userSliceLogin.userLogin);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      id: mainUser?.id,
      email: mainUser?.email,
      name: mainUser?.email,
      phoneNumber: mainUser?.email,
      password: "",
      passwordConfirmation: "",
    },
  });

  if (!formik.isValid) return;
  const handleSubmit = () => {
    formik.setTouched({
      email: true,
      name: true,
      phoneNumber: true,
      password: true,
      passwordConfirmation: true,
    });

    dispatch(
      editUser(formik.values, () => {
        setTimeout(() => {
          formik.resetForm();
        }, 500);
      }).then((res) => {
        message.success("Thành Công");
        window.location.reload();
      })
    );
  };

  const handleCancel = () => {
    formik.resetForm();
  };

  return (
    <div className="w-full mx-auto px-auto">
      <Row className="ml-10">
        <Col span={8}>
          <Avatar size={250} src={mainUser?.avatar} alt={mainUser?.name} />
        </Col>
        <Col span={8}>
          <Form layout="vertical" onFinish={handleSubmit}>
            <Form.Item
              label={
                <Typography.Text strong>
                  Id <span className="text-red-700">*</span>
                </Typography.Text>
              }
            >
              <Input name="id" value={formik.values.id} disabled />
            </Form.Item>

            <Form.Item
              label={
                <Typography.Text strong>
                  Email <span className="text-red-700">*</span>
                </Typography.Text>
              }
              help={formik.touched.email && formik.errors.email}
              validateStatus={
                formik.touched.email && !!formik.errors.email ? "error" : ""
              }
            >
              <Input
                name="email"
                value={formik.values.email}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Form.Item>

            <Form.Item
              label={
                <Typography.Text strong>
                  Name <span className="text-red-700">*</span>
                </Typography.Text>
              }
              help={formik.touched.name && formik.errors.name}
              validateStatus={
                formik.touched.name && !!formik.errors.name ? "error" : ""
              }
            >
              <Input
                name="name"
                value={formik.values.name}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Form.Item>

            <Form.Item
              label={<Typography.Text strong>Phone number</Typography.Text>}
              help={formik.touched.phoneNumber && formik.errors.phoneNumber}
              validateStatus={
                formik.touched.phoneNumber && !!formik.errors.phoneNumber
                  ? "error"
                  : ""
              }
            >
              <Input
                name="phoneNumber"
                value={formik.values.phoneNumber}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Form.Item>

            <Form.Item
              label={
                <Typography.Text strong>
                  Password <span className="text-red-700">*</span>
                </Typography.Text>
              }
              help={formik.touched.password && formik.errors.password}
              validateStatus={
                formik.touched.password && !!formik.errors.password
                  ? "error"
                  : ""
              }
            >
              <Input
                type="password"
                name="password"
                value={formik.values.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Form.Item>

            <Form.Item
              label={
                <Typography.Text strong>
                  Password confirmation <span className="text-red-700">*</span>
                </Typography.Text>
              }
              help={
                formik.touched.passwordConfirmation &&
                formik.errors.passwordConfirmation
              }
              validateStatus={
                formik.touched.passwordConfirmation &&
                !!formik.errors.passwordConfirmation
                  ? "error"
                  : ""
              }
            >
              <Input
                type="password"
                name="passwordConfirmation"
                value={formik.values.passwordConfirmation}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Form.Item>

            <Form.Item className="mb-0 text-right">
              <Button
                htmlType="submit"
                className="bg-blue-700 hover:bg-blue-600 focus:bg-blue-700 text-white font-semibold hover:text-white focus:text-white border-blue-700 hover:border-blue-600 focus:border-blue-700 rounded mr-1"
                onClick={handleCancel}
              >
                Update
              </Button>
              <Button
                className="hover:bg-gray-200 text-gray-700 hover:text-gray-700 font-semibold border-transparent hover:border-gray-200 rounded shadow-none"
                onClick={handleCancel}
              >
                Cancel
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </div>
  );
}
