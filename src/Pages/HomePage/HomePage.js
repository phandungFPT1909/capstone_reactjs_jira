import React from 'react'
import "../../index.css"
import JiraMenu from './../../Components/JiraMenu/JiraMenu';
import HeaderMain from '../../Components/JiraMain/HeaderMain';
import InfoMain from '../../Components/JiraMain/InfoMain';
import ContentMain from '../../Components/JiraMain/ContentMain';
import JiraModal from '../../Components/JiraModal/JiraModal';
export default function HomePage() {
  return (
   <>
    <div className="jira">
        <JiraMenu />
    
      {/* {/* {/* Main Board * /} * /} */}
        <div className="main">
          <HeaderMain />
          <h3>Cyber Board</h3>
          <InfoMain />
          <ContentMain />
        </div>
    </div>
    <JiraModal />
  </>

  )
}
