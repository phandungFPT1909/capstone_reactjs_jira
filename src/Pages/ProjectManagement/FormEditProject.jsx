import React from 'react'
import { Editor } from "@tinymce/tinymce-react";
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';
import { message, Space, Button } from "antd";
import { getProjectCategory, putUpdateProject } from '../../service/createProjectService';
import { putUpdateProjectAction, setArrUpdateProject } from '../../redux-toolkit/updateProjectSlice';
import { useState, useEffect } from 'react';
import { setProjectCategoryInfo } from '../../redux-toolkit/ProjectCategorySlice';
import { useNavigate } from 'react-router-dom';

export default function FormEditProject(props) {
    const dispatch = useDispatch()
    const [contents, setContent] = useState("")

    const arrProjectCategory = useSelector((state) => {
        return state.projectCategorySlice.arrProjectCategory
      })

    const ArrUpdateProject = useSelector((state) => {
        return state.updateProjectSlice.ArrUpdateProject
      })

    useEffect(() => {
        // gọi API để lấy dữ liệu thẻ select
        getProjectCategory()
        .then((res) => {
          dispatch(setProjectCategoryInfo(res.data.content))
        })
        .catch((err) => {
          console.log(err);
        })

      }, []);


    const handleEditorChange = (content, editor) => {
        var contentNew = content;
        setContent(contentNew);
      };

      const formik = useFormik({
        initialValues: {
          id:ArrUpdateProject?.id,
          projectName:ArrUpdateProject?.projectName,
          description:ArrUpdateProject?.description,
          categoryId:ArrUpdateProject?.categoryId,
        },
    
        onSubmit: (values) => {
          let request = {...values,description:contents }

          putUpdateProject(request)
          .then((res) => {
            dispatch(setArrUpdateProject(res.data?.content))
            console.log("res.data.content",res.data?.content);
            message.success("update thành công")
            props.listProject()
           
          })
          .catch((err) => {
            console.log(err);
            message.success("update thất bại");
          })
          
        },
      });
    
  return (
    <form className='container-full' onSubmit={formik.handleSubmit}>
        <div className='row'>
            <div className='col-4'>
                  <div classname="form-group">
                    <h2 className='text-xl text-blue-600'>ID</h2>
                    <input disabled className="form-control" name="id" onChange={formik.handleChange} value={formik.values.id} />
                </div>
            </div>

            <div className='col-4'>
                <div classname="form-group">
                  <h2 className='text-xl text-blue-600'>Project Name</h2>
                  <input className="form-control" name="projectName" onChange={formik.handleChange} value={formik.values.projectName} />
                </div>
            </div>

            <div className='col-4'>
                <div className="form-group">
                <h2 className='text-xl text-blue-600'>Project Category</h2>
                    <select name="categoryId" className="form-control" onChange={formik.handleChange} value={formik.values.categoryId}>
                    {arrProjectCategory.map((item,index) => {
                        return <option value={item.id} key={index}>{item.projectCategoryName}</option>
                    })}
                    </select>
                </div>
            </div>
            </div>

            <div className='row'>

           <div className="col-12">
            <div className="form-group">
             <h2 className='text-xl text-blue-600'>Description</h2>
                <Editor
                  name="description"
                  initialValue={formik.values.description}
                  // value={formik.values.description}
                  init={{
                    height: 450,
                    menubar: false,
                    plugins: [
                      "a11ychecker",
                      "advlist",
                      "advcode",
                      "advtable",
                      "autolink",
                      "checklist",
                      "export",
                      "lists",
                      "link",
                      "image",
                      "charmap",
                      "preview",
                      "anchor",
                      "searchreplace",
                      "visualblocks",
                      "powerpaste",
                      "fullscreen",
                      "formatpainter",
                      "insertdatetime",
                      "media",
                      "table",
                      "help",
                      "wordcount",
                    ],
                    toolbar:
                      "undo redo | casechange blocks | bold italic backcolor | " +
                      "alignleft aligncenter alignright alignjustify | " +
                      "bullist numlist checklist outdent indent | removeformat | a11ycheck code table help",
                  }}
                  onEditorChange={handleEditorChange}
                />
           </div>
         </div>
     </div>

         <div>
              <button className='border-2 rounded px-5 font-medium btn btn-outline-primary mr-5' onClick={() => {
                props.onClose()
              }}>Cancel</button>
              <button className="border-2 rounded px-5 font-medium btn btn-outline-primary" onClick={() => {
                props.showDrawer()
              }} type="submit">
                Submit
              </button>
         </div>

    </form>
  )
}
