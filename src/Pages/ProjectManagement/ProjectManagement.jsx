import React from 'react'
import { Avatar, Button, message, Table, Popover, AutoComplete, } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { useState } from 'react';
import "./styleProjectManagement.css"

// ES Modules
import { deleteProject, getAllProject } from '../../service/createProjectService';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { Tag } from 'antd';
import { setArrUpdateProject } from '../../redux-toolkit/updateProjectSlice';
import { Drawer, Select } from 'antd';
import FormEditProject from './FormEditProject';
import { setUserInfoSearch } from '../../redux-toolkit/SearchUserSlice';
import { potAddUser } from '../../service/userService';
import { postRemoveUser, getSearchUser } from './../../service/userService';

export default function ProjectManagement() {
  const dispatch = useDispatch()
  const [value, setValue] = useState("")

  const searchUser = useSelector((state) => {
    return state.userSliceSearch.searchUser
  })

   const [arrAllProject, setArrAllProject] = useState([])
    console.log("arrAllProject",arrAllProject);

  const [open, setOpen] = useState(false);
  const [display, setDisplay] = useState(false);

  const showDrawer = (item) => {
    setDisplay(true);
    setOpen(true);

    // gửi dữ liệu lên redux
    dispatch(setArrUpdateProject(item))

  };

  const onClose = () => {
    setDisplay(false);
    setOpen(false);
  };

const renderFormEdit = () =>{
  if (display) {
    return ( <Drawer
      title="Create a new account"
      width={720}
      onClose={onClose}
      open={open}
      bodyStyle={{
        paddingBottom: 80,
      }}
    >
      <FormEditProject onClose={onClose} showDrawer={showDrawer} listProject={listProject}/>

    </Drawer>)
  }
}


const listProject = () => {
  getAllProject()
        .then((res) => {
          let arrProjectList = res.data.content.map((item) => {
            return {
              ...item,
              key: item.id,
              action: (
                <>
                  <button onClick={() => {showDrawer(item)}} className='mr-5 text-blue-600 hover:text-blue-700 style-button'><EditOutlined /></button>
                  <button onClick={() => {
                    {handleDeleteProject(item?.id)}
                  }} className='text-red-600 hover:text-red-700 style-button'><DeleteOutlined /></button>
                </>
              )
            }
          })
          // dispatch(setListProjectInfo(arrProjectList))
          setArrAllProject(arrProjectList)
        })
        .catch((err) => {
          console.log("err", err);
        })
        }

        const handleDeleteProject = (idProject) => {
          deleteProject(idProject)
                .then((res) => {
                  message.success("xóa thành công")
                  listProject()
                })
                .catch((err) => {
                  console.log("err", err);
                  message.success("xóa thất bại")
                })
            }

  useEffect(() => {
            listProject()
  }, []);


  const columns = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id',
      sorter: (item2,item1) => {
         return item2.id - item1.id 
      }
    },
    {
      title: 'projectName',
      dataIndex: 'projectName',
      key: 'projectName',
      sorter: (item2,item1) => {
        let projectName1 = item1.projectName?.trim().toLowerCase()
        let projectName2 = item2.projectName?.trim().toLowerCase()
        if(projectName2 < projectName1) {
            return -1;
        }
        return 1;
      }
    },

    {
        title: 'category',
        dataIndex: 'categoryName',
        key: 'categoryName',
        sorter: (item2,item1) => {
            let category1 = item1.category?.trim().toLowerCase()
            let category2 = item2.category?.trim().toLowerCase()
            if(category2 < category1) {
                return -1;
            }
            return 1;
          }
      },

      {
        title: 'creator',
        // dataIndex: 'creator',
        key: 'creator',
        sorter: (item2,item1) => {
            let creator1 = item1.creator.name?.trim().toLowerCase()
            let creator2 = item2.creator.name?.trim().toLowerCase()
            if(creator2 < creator1) {
                return -1;
            }
            return 1;
          },
        render: (text,record,index) => {
            return <Tag color="green">{record.creator?.name}</Tag>
        }
        
      },

    {
      title: "members",
      key: "members",
      render: (text,record,index) => {
        return <div>
          {record.members?.slice(0,3).map((member,index) => {
            return (
            <Popover key={index} placement="top" title="members" content={() =>{
              return <table className='table'>
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Avatar</th>
                    <th>name</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {record.members?.map((item,index) => {
                   return <tr key={index}>
                      <td>{item.userId}</td>
                      <td><img src={item.avatar} width="30" height="30" style={{borderRadius:"15px"}} /></td>
                      <td>{item.name}</td>
                      <td>
                        <button onClick={() => {
                          // gọi api gửi về backend
                          const arrIdName = {
                            "userId": item.userId,
                            "projectId": record.id,
                          }
                          postRemoveUser(arrIdName)
                          .then((res) => {
                            message.success("Xóa thành công");
                            listProject();
                          })
                          .catch((err) => {
                            console.log("err", err);
                            message.success("Xóa Thất bại");
                          })

                        }} className='text-red-600 hover:text-red-700 style-button'><DeleteOutlined /></button>
                      </td>
                    </tr>
                  })}
                </tbody>
              </table>
            }}>
               <Avatar key={index} src={member.avatar} />
            </Popover>
            )
          })}
          {record.members?.length>3? <Avatar>...</Avatar> : ""}

          <Popover placement="topLeft" title={"Add user"} content={() => {
            return <AutoComplete
            options={searchUser?.map((user,index) => {
              return {label:user.name,value:user.userId.toString()}
            })}

            value={value}
           
            onChange={(text) => {
              setValue(text);
            }}
            onSelect={(valueSelect,option) => {
            //  set giá trị hộp thoại = option.label
            setValue(option.label);

            // gọi api gửi về backend
            const arrIdName = {
              "projectId": record.id,
              "userId": valueSelect
            }
          
            potAddUser(arrIdName)
                    .then((res) => {
                      message.success("thêm thành công");
                      listProject();
                    })
                    .catch((err) => {
                      console.log("err", err);
                      message.success("thêm Thất bại");
                    })
      
            }}

             style={{width:"100%"}} onSearch={(value) => {
              console.log("valueSearch",value);
              getSearchUser(value)
              .then((res) => {
                console.log("res",res);
                dispatch(setUserInfoSearch(res.data.content))
              })
              .catch((err) => {
                console.log("err",err);
              })
              
            }}>
              acb
            </AutoComplete>
          }} trigger="click">
             <Button style={{borderRadius:"60%"}}>+</Button>
         </Popover>
        </div>
    }
    },

    {
        title: 'Action',
        dataIndex:"action",
        key: 'action',
      },
  ];

  
  return (
    <>
    <div className="main w-full mt-5">
      <h2>Project Management</h2>
      <Table columns={columns} rowKey={"id"} dataSource={arrAllProject} />;
    </div>
    
       {renderFormEdit()}
    </>
  )
}
