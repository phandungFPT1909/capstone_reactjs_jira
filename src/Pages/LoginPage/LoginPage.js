import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { postLogin } from "./../../service/userService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { userLocalService } from "../../service/localService";
import "./styleLogin.css";
import { NavLink } from "react-router-dom";
import { setUserInfoLogin } from "../../redux-toolkit/userSliceLogin";

export default function LoginPage() {
  let navigate = useNavigate();
  // phương thưc gửi dữ liệu lên redux
  let dispatch = useDispatch();
  const onFinish = (values) => {
    console.log("Success:", values);
    // chuyển đổi thành key có tài khoản với mật khẩu trùng trên API
    let userLogin = {
      email: values.Email,
      passWord: values.password,
    };
    postLogin(userLogin)
      .then((res) => {
        // gửi dữ liệu lên redux
        dispatch(setUserInfoLogin(res.data.content))
        // Lưu xuống local
        userLocalService.set(res.data.content);

        // chuyển hướng về trang chủ sau khi đăng nhập thành công
        setTimeout(() => {
          window.location.href="/projectmanagement"
        }, 2000);
        // tạo Từ antd
        message.success("Đăng nhập thành công");
      })
      .catch((err) => {
        // tạo Từ antd
        message.success("Đăng nhập thất bại");
        console.log(err);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="flex">
       <div className="login-left w-full">
        <img className="h-screen" src="" alt="" />
        </div>

        <div className="login px-10 p-5 bg-white rounded flex justify-center items-center login-right">
          <div className="p-5 rounded login-right-content">
            <Form
              layout="vertical"
              name="basic"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 24,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item>
                <div className="login__icon">
                  <i class="fa fa-user"></i>
                </div>
                <h2 className="text-2xl font-medium text-center">Đăng nhập</h2>
              </Form.Item>
              
              {/* email */}
              <Form.Item
              label="Email"
              name="Email"
              rules={[
                {
                  required: true,
                  message: "Please input your Email",
                },
              ]}
            >
              <Input placeholder="alice3422@gmail.com" />
            </Form.Item>

              <Form.Item
                label="Mật khẩu"
                name="password"
                rules={[
                  {
                    required: true,
                    message: "Please input your password!",
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item
                name="remember"
                valuePropName="checked"
                wrapperCol={{
                  offset: 0,
                  span: 16,
                }}
              >
                <Checkbox>Nhớ tài khoản</Checkbox>
              </Form.Item>

              <Form.Item
                wrapperCol={{
                  span: 24,
                }}
                className="text-center"
              >
                <Button
                  className="bg-blue-500 text-white px-20 font-medium"
                  htmlType="submit"
                >
                  Đăng nhập
                </Button>
              </Form.Item>

              <Form.Item>
                <NavLink to="/signup">
                  <p className="text-blue-500 text-base font-medium">
                    Bạn chưa có tài khoản? Đăng ký
                  </p>
                </NavLink>
              </Form.Item>

              <Form.Item>
                <p className="text-blue-500 text-base font-medium text-center">
                  Đăng nhập bằng facebook
                </p>

                <NavLink>
                  <p className="text-blue-500 text-2xl text-center mb-0"><i class="fab fa-facebook"></i></p>
                </NavLink>
              
            </Form.Item>

            </Form>
          </div>
        </div>
      </div>
   
  );
}
