import React from 'react'
import { NavLink } from 'react-router-dom'
import { useNavigate } from 'react-router-dom';

export default function NotFoundPage() {
   let navigate = useNavigate();

let handleChangeHome = () => {
  setTimeout(() => {
    // window.location => load lại trang nên không dùng
    // window.location.href = "/"
    navigate("/")
  },3000)
}

  return (
    <div className='h-screen w-screen flex flex-col justify-center items-center'>
       <h1 className='animate-bounce text-2xl'>Không tìm thấy trang này</h1>
        <h2 className='text-center text-red-600 font-black animate-bounce text-2xl'>404 Page</h2>
       
        {/* <NavLink to={setTimeout}> */}
           <button onClick={handleChangeHome} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Quay về trang chủ</button>
        {/* </NavLink> */}
    </div>
  )
}

// tìm đọc tailwind animation