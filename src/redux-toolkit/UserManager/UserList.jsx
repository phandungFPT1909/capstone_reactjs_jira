import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  arrUserManager: [],
  arrUserUpdate: [],
};

const getUserSlice = createSlice({
  name: "getUserSlice",
  initialState,
  reducers: {
    setGetUserInfo: (state, action) => {
      state.arrUserManager = action.payload;
    },
    setGetUserValue: (state, action) => {
      state.arrUserUpdate = action.payload;
    },
  },
});
export const { setGetUserInfo, setGetUserValue } = getUserSlice.actions;
export default getUserSlice.reducer;
