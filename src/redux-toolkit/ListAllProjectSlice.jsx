import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    arrAllProject:[],
    
  };

  const listProjectSlice = createSlice({
    name:"listProjectSlice",
    initialState,
    reducers:{
        setListProjectInfo:(state,action) => {
            state.arrAllProject = action.payload;
            console.log("listProject",action.payload);
             
        },
    }
  })

  export const {setListProjectInfo} = listProjectSlice.actions
  export default listProjectSlice.reducer;

