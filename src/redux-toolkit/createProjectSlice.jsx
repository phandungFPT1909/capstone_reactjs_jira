import { createSlice } from "@reduxjs/toolkit";
import { postCreateProject } from "../service/createProjectService";
import { message } from "antd";
const initialState = {
    createProject:null
  };

  const createProjectSlice = createSlice({
    name:"createProjectSlice",
    initialState,
    reducers:{
        setCreateProject:(state,action) => {
            state.createProject = action.payload;
             
        },
    }
  })

  export const {setCreateProject} = createProjectSlice.actions
  export default createProjectSlice.reducer;

