import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    isLoading:false,
}

const SpinnerSlice = createSlice({
  name: "SpinnerSlice",
  initialState,
  reducers: {
    setLoadingOn:(state,payload) => { 
        state.isLoading=true;
     },
     setLoadingOff:(state,payload) => { 
        state.isLoading=false;
     }
  }
});

export const {setLoadingOn,setLoadingOff} = SpinnerSlice.actions

export default SpinnerSlice.reducer