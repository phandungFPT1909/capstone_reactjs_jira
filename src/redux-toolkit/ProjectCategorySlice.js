import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    arrProjectCategory:[],
    
  };

  const projectCategorySlice = createSlice({
    name:"projectCategorySlice",
    initialState,
    reducers:{
        setProjectCategoryInfo:(state,action) => {
            state.arrProjectCategory = action.payload;
             
        },
    }
  })

  export const {setProjectCategoryInfo} = projectCategorySlice.actions
  export default projectCategorySlice.reducer;


  