import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    userSignUp: null,
  };

  const userSliceSignUp = createSlice({
    name:"userSliceSignUp",
    initialState,
    reducers:{
        setUserInfoSignUp:(state,action) => {
            state.userSignUp = action.payload;
        }
    }
  })

  export const {setUserInfoSignUp} = userSliceSignUp.actions
  export default userSliceSignUp.reducer;