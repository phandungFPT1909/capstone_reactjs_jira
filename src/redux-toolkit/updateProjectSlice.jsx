import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  ArrUpdateProject: null
}

  const updateProjectSlice = createSlice({
    name:"updateProjectSlice",
    initialState,
    reducers:{
        setArrUpdateProject:(state,action) => {
            state.ArrUpdateProject = action.payload;
        },
    }
  })

  export const {setArrUpdateProject} = updateProjectSlice.actions
  export default updateProjectSlice.reducer;

