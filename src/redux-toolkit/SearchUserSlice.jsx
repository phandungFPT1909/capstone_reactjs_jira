import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    searchUser: [],
  };

  const userSliceSearch = createSlice({
    name:"userSliceSearch",
    initialState,
    reducers:{
        setUserInfoSearch:(state,action) => {
            state.searchUser = action.payload;
        }
    }
  })

  export const {setUserInfoSearch} = userSliceSearch.actions
  export default userSliceSearch.reducer;


