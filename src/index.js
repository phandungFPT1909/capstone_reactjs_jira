import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";

// antd
import "antd/dist/reset.css";
// redux thunk
import thunk from "redux-thunk";
// slick
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { configureStore } from "@reduxjs/toolkit";
import userSliceLogin from "./redux-toolkit/userSliceLogin";
import userSliceSignUp from "./redux-toolkit/userSliceSignUp";
import projectCategorySlice from "./redux-toolkit/ProjectCategorySlice";
import createProjectSlice from "./redux-toolkit/createProjectSlice";
import listProjectSlice from "./redux-toolkit/ListAllProjectSlice";
import updateProjectSlice from "./redux-toolkit/updateProjectSlice";
import userSliceSearch from "./redux-toolkit/SearchUserSlice";
import SpinnerSlice from "./redux-toolkit/SpinnerSlice";
import getUserSlice from "./redux-toolkit/UserManager/UserList";
const root = ReactDOM.createRoot(document.getElementById("root"));

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store_toolkit = configureStore({
  reducer: {
    userSliceLogin: userSliceLogin,
    userSliceSignUp: userSliceSignUp,
    projectCategorySlice: projectCategorySlice,
    createProjectSlice: createProjectSlice,
    listProjectSlice: listProjectSlice,
    updateProjectSlice: updateProjectSlice,
    userSliceSearch: userSliceSearch,
    SpinnerSlice: SpinnerSlice,
    getUserSlice: getUserSlice,
  },
});
root.render(
  <Provider store={store_toolkit}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
