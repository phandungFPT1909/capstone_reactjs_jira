import React from 'react'
import JiraMenu from '../Components/JiraMenu/JiraMenu'

export default function Layout({children}) {
  return (
    <div className="jira">
        <JiraMenu />
        {children}
    </div>
  )
}
