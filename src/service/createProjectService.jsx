import { https } from "./configURL";

export let getProjectCategory = () => {
    return https.get("/api/ProjectCategory");
  };

export let postCreateProject = (data) => {
    return https.post("/api/Project/createProjectAuthorize", data);
  };

  export let getAllProject = () => {
    return https.get("/api/Project/getAllProject");
  };

  export let deleteProject = (id) => {
    return https.delete(`/api/Project/deleteProject?projectId=${id}`);
    
  };
  export let putUpdateProject = (data) => {
    return https.put(`/api/Project/updateProject?projectId=${data.id}`,data);
  };
  