import { https } from "./configURL";

export let getUserArr = () => {
  return https.get("/api/Users/getUser");
};

export const deletUser = async (id) => {
  try {
    const response = await https.delete(`/api/Users/deleteUser?id=${id}`);
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export let editUser = (data) => {
  return https.put(`/api/Users/editUser`, data);
};
