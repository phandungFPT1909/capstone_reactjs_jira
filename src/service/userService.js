import { https } from "./configURL";

export let postLogin = (data) => {
  return https.post("/api/Users/signin", data);
};

export let postRegister = (data) => {
  return https.post("/api/Users/signup", data);
};
export let getSearchUser = (keyword) => {
  return https.get(`/api/Users/getUser?keyword=${keyword}`);
};
export let potAddUser = (data) => {
  return https.post("/api/Project/assignUserProject",data);
};
export let postRemoveUser = (data) => {
  return https.post("/api/Project/removeUserFromProject",data);
};


