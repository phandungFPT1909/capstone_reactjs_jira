import axios from 'axios'
import { userLocalService } from './localService';
import { store_toolkit } from './../index';
import { setLoadingOff, setLoadingOn } from '../redux-toolkit/SpinnerSlice';
const TOKEN_CYBERSOFT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzNSIsIkhldEhhblN0cmluZyI6IjAzLzA2LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY4NTc1MDQwMDAwMCIsIm5iZiI6MTY1NzczMTYwMCwiZXhwIjoxNjg1ODk4MDAwfQ.KXn1XtehbphvfW3OSUFlLIzSrEtSLDtDQG4BgF38Cus"

export const https = axios.create({
    baseURL: 'https://jiranew.cybersoft.edu.vn',
    headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + userLocalService.get()?.accessToken

    },
  });

  
// Add a request interceptor
https.interceptors.request.use(function (config) {
    console.log("start");
    store_toolkit.dispatch(setLoadingOn());
    // Do something before request is sent
    return config;
  }, function (error) {
    // store_toolkit.dispatch(setLoadingOff());
    // Do something with request error
    return Promise.reject(error);
  });

// Add a response interceptor
https.interceptors.response.use(function (response) {
    console.log("end");
    store_toolkit.dispatch(setLoadingOff());
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  }, function (error) {
    store_toolkit.dispatch(setLoadingOff());
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  });
